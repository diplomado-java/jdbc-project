package com.java.curso;


import com.java.curso.util.ConnectionJDBC;

import java.sql.*;

public class SampleJDBC {

    public static void main(String[] args) {
        String url = "jdbc:mysql://localhost:3306/java_curso";
        String username = "root";
        String password = "";

        try (Connection conn = DriverManager.getConnection(url, username, password);
             Statement statement = conn.createStatement();
             ResultSet rs = statement.executeQuery("SELECT * FROM productos")){
            while (rs.next()) {
                System.out.println(rs.getString("nombre"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
