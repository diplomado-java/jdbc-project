package com.java.curso.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionJDBC {
    private static final String url = "jdbc:mysql://localhost:3306/java_curso";
    private static final String user = "root";
    private static final String password = "";
    private static Connection conn;

    private ConnectionJDBC() {

    }

    public static Connection getInstance() throws SQLException {
        if (conn == null) {
            conn = DriverManager.getConnection(url, user, password);
        }
        return conn;
    }
}
